package com.jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jpa.dao.EmployeeDAO;
import com.jpa.dao.EmployeeImpl;
import com.jpa.model.Stock;

@Controller
public class HomeController {
	
	@Autowired
	EmployeeDAO dao;
	ModelAndView mv;
	
	@GetMapping("/add")
	String add() {
		System.out.println("get:/add");
		return "add";
	}

//	@RequestMapping("/display")
//	ModelAndView display() {
//		System.out.println("requestmapping:/display");
//		mv = new ModelAndView();
//		List<Stock> stocks = dao.getStock();
//		mv.addObject("stocks", stocks);
//		mv.setViewName("display");
//		return mv;
//	}

	@RequestMapping("/display")
	@ResponseBody
	List<Stock> display() {
		System.out.println("requestmapping:/display");
		List<Stock> stocks = dao.getStock();
		return stocks;
	}
	
	@PostMapping("/add")
	ModelAndView addData(@RequestParam String stockName,@RequestParam String stockCode) {
		System.out.println("post:/add");
		mv = new ModelAndView();
		dao.add(stockName,stockCode);
		mv.setViewName("redirect:display");
		return mv;
	}
	
	@GetMapping("/delete")
	String delete(@RequestParam("stockId") String stockId) {
		System.out.println("get:/delete");
		dao.delete(stockId);
		return "redirect:display";
	}
}
