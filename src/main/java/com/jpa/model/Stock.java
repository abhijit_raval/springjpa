package com.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="stock", schema = "mydatabase")

public class Stock implements Serializable{
	
	@Override
	public String toString() {
		return "Stock [STOCK_ID=" + stockId + ", stockCode=" + stockCode + ", stockName=" + stockName + "]";
	}

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="STOCK_ID")
	int stockId;
	 
	@Column(name="STOCK_CODE")
	String stockCode;
	
	@Column(name="STOCK_NAME")
	String stockName;

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
}
