package com.jpa.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class JpaAspect {
	
	//@Pointcut("execution(* com.jpa.controller.HomeController.display(..))") //Point-cut expression  
	@Pointcut("execution(* com.jpa.dao.EmployeeImpl.getStock(..))") //Point-cut expression  
	public void globalAspect() {}

	@Before("globalAspect()")
	void logBeforeV1() {
		System.out.println("logBeforeV1()");
	}

	@After("globalAspect()")
	void logBeforeV2() {
		System.out.println("logBeforeV2()");
	}
}
