package com.jpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.stereotype.Service;

import com.jpa.model.Stock;

@Service
public class EmployeeImpl implements EmployeeDAO {
	EntityManagerFactory emFactory;
	EntityManager entityManager;
	Stock stock = new Stock();

	void getEntity() {
		emFactory = Persistence.createEntityManagerFactory("Eclipselink_JPA");
		entityManager = emFactory.createEntityManager();
	}

	void closeEntity() {
		entityManager.getTransaction().commit();
		entityManager.close();
		emFactory.close();
	}

	public void add(String stockName, String stockCode) {
		// TODO Auto-generated method stub
		getEntity();
		System.out.println(stockCode + "" + stockName);
		entityManager.getTransaction().begin();
		stock.setStockName(stockName);
		stock.setStockCode(stockCode);
		entityManager.merge(stock);
		closeEntity();
	}

	public void update() {
		// TODO Auto-generated method stub

	}

	public List<Stock> getStock() {
		// TODO Auto-generated method stub
		System.out.println("In getstock()");
		getEntity();
		return entityManager.createQuery(" FROM Stock").getResultList();
	}

	public void delete(String stockId) {
		// TODO Auto-generated method s tub
		getEntity();
		entityManager.getTransaction().begin();
		stock = entityManager.find(Stock.class, Integer.parseInt(stockId));
		entityManager.remove(stock);
		closeEntity();
	}
}
