package com.jpa.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jpa.model.Stock;

@Service
public interface EmployeeDAO {
	void add(String stockName, String stockCode);
	void update();
	void delete(String stockId);
	List<Stock> getStock();
}
