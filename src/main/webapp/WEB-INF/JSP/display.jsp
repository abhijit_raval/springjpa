<%@page import="java.io.Console"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> 
    <style>
        tr:first-child{
            font-weight: bold;
            background-color: #C6C9C4;
        }
    </style>

</head>
<body>
<center style="margin:50px auto">
	<h2>List of stock</h2>
	<table>
		<tr>
			<td>StockID</td>
			<td>Stock Code</td>
			<td>Stock Name</td>
			<td></td>
		</tr>
		<c:forEach items="${stocks}" var="stock">
			<tr>
				<td>${stock.stockId}</td>
				<td>${stock.stockCode}</td>
				<td>${stock.stockName}</td>
				<td><a href="<c:url value='/delete?stockId=${stock.stockId}'/>">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<a href="<c:url value='add' />">Add New Stock</a>
	</center>
</body>
</html>